# View-Statistics Erweiterung für TYPO3

Diese Erweiterung fügt Statistikdatensätze in jede Seitenansicht ein. Diese Erweiterung verwendet keine Cookies!


>   #### Achtung: {.alert .alert-danger}
>   
>   Diese Erweiterung trackt nicht, wenn Du gleichzeitig mit einem Backend-Benutzer angemeldet bist und das Frontend mit demselben Domainnamen aufrufst. Verwende in diesem Fall einen anderen Browser für den Aufruf des Frontends, um das Tracking auszulösen! Selbst ein Inkognito-Fenster des gleichen Browsers kann u. U. verhindern, dass das Tracking ausgeführt wird.


**Features:**

*   Konfiguriere, wie getrackt werden soll (konfigurierbar im Extensionmanager: alle Seitenaufrufe - angemeldete und nicht angemeldete Besucher, nur angemeldete Besucher oder nur nicht angemeldete Besucher)
*   Optional: Tracking der ID des angemeldeten Benutzers
*   Optional: Tracking der IP-Adresse
*   Optional: Tracking der Anmeldezeiten für angemeldete Frontend-Benutzer
*   Optional: Tracking des User Agents (z. B. welcher Browser benutzt wurden)
*   Backend-Modul mit: Übersicht, Listings, CSV-Export und Benutzereinschränkung
*   Tracking für Seiten und Objekte wie: Anzeigen von Nachrichten (EXT:news), Downloads (EXT:downloadmanager), Produkten (EXT:shop), Immobilien (EXT:openimmo)
*   Konfiguration Deines eigenen Objekts per TypoScript

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [View-Statistics Dokumentation](https://www.coding.ms/documentation/typo3-view-statistics "View-Statistics Dokumentation")
*   [View-Statistics Bug-Tracker](https://gitlab.com/codingms/typo3-public/view_statistics/-/issues "View-Statistics Bug-Tracker")
*   [View-Statistics Repository](https://gitlab.com/codingms/typo3-public/view_statistics "View-Statistics Repository")
*   [TYPO3 View-Statistics Produktdetails](https://www.coding.ms/typo3-extensions/typo3-view-statistics "TYPO3 View-Statistics Produktdetails")
*   [TYPO3 View-Statistics Download](https://extensions.typo3.org/extension/view_statistics/ "TYPO3 View-Statistics Download")
