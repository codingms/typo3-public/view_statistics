# View-Statistics Besucherzähler
Die View-Statistics Erweiterung zählt automatisch die Anzahl der Besucher Deiner Webseite.
Die Anzahl der Besucher findst Du im Backend-Modul *Pages*. Unter der Root-Seite klickst Du auf
*Edit page properties* und im Tab *SEO* findest Du das Feld *visitors*.

## Frontend Plugin
Die Anzahl der Besucher kann auch im Frontend mithilfe des Plugins *Visitors* angezeigt werden.
