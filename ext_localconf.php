<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'ViewStatistics',
    'Visitors',
    [\CodingMs\ViewStatistics\Controller\VisitorsController::class => 'show'],
    [\CodingMs\ViewStatistics\Controller\VisitorsController::class => 'show']
);

//
// Backend TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:view_statistics/Configuration/TypoScript/Backend/setup.typoscript">'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:view_statistics/Configuration/PageTS/tsconfig.typoscript">'
);
