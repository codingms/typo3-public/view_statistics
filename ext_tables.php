<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

// Registers a Backend Module
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'ViewStatistics',
        'web',
        'viewstatistics',
        '',
        [
            \CodingMs\ViewStatistics\Controller\BackendController::class => 'list,listForUser,listForPage,listForObject,userStatistics,statistic,listForSummary',
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:view_statistics/Resources/Public/Icons/module-viewstatistics.svg',
            'labels' => 'LLL:EXT:view_statistics/Resources/Private/Language/locallang_db.xlf',
        ]
    );
}
