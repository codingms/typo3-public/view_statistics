<?php

return [
    'dependencies' => ['core', 'backend'],
    'tags' => [
        'backend.form',
    ],
    'imports' => [
        '@codingms/view-statistics/backend/datetime-picker.js' => 'EXT:view_statistics/Resources/Public/JavaScript/Backend/DateTimePicker.js',
    ],
];
