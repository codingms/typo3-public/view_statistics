<?php

defined('TYPO3') or die();

call_user_func(function () {
    $visitors = [
        'visitors' => [
            'label' => 'Visitors',
            'displayCond' => 'FIELD:is_siteroot:=:1',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,int'
            ]
        ]
    ];
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'pages',
        $visitors
    );
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
        'pages',
        'seo',
        'visitors'
    );
});
