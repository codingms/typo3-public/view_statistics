import DocumentService from "@typo3/core/document-service.js";
import DateTimePicker from "@typo3/backend/date-time-picker.js";

class DateTimePickerViewStatistics {

    initialize() {
        top.TYPO3.DateTimePickerViewStatistics = this;
        DocumentService.ready().then(() => {
            if (window.self !== window.top) {
                this.onlyInContentFrame();
            }
        });
    }

    onlyInContentFrame() {
        this.initializeDateTimePickerElements();
    }

    initializeDateTimePickerElements() {
        document.querySelectorAll(".t3js-datetimepicker").forEach((e => DateTimePicker.initialize(e)))
    }

}

export default new DateTimePickerViewStatistics();
